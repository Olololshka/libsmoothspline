
#ifndef _SMOOTH_SPLINE_
#define _SMOOTH_SPLINE_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

typedef struct {
  double A, B, C, D;
  double Xmin;
  double Xmax;
} spline_range;

typedef struct {
  double Y, d1Y, d2Y;
} calc_result;

/*	--------------------------------------------------------*/
/*		функция сплайн-аппроксимации набора точек */
/*		pX - указатель на массив аргумента
 */
/*		pY - указатель на массив значений функции */
/*		pg - коэффициент сглаживания (число от 0 до 1000)	*/
/*		NWin - число точек окна аппроксимации */
/*	--------------------------------------------------------*/

/** EXAMPLE
 *
 *  spline_range *spline;
 *  SplineApprox(mX, mY, sizeof(mX) / sizeof(double), 200, &spline);
 *
 *  ...
 *
 *  delete spline;
 */

void SplineApprox(double *pX, double *pY, uint32_t NWin, double pg,
                  spline_range **result);

/** EXAMPLE
 *
 *  double X = XStart;
 *  while(X <= XEnd)
 *  {
 *      calc_result result = calculateSplineAt(spline, SIZE_OF(spline), X);
 *      printf("X = %f, Y = %f, d1y = %f, d2y = %f", X, result.Y, result.d1Y,
 * result.d2Y); X += STEP;
 *  }
 */

calc_result calculateSplineAt(spline_range spline[], int size, double x);

#ifdef __cplusplus
}
#endif

#endif
