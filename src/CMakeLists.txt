cmake_minimum_required(VERSION 2.8)

#инклюды для сборки (-I<путь>)
include_directories(include
    include
    )

#список хедеров проекта (не Q_OBJECT'ов!)
set(${PROJECT_NAME}_C_HDRS
    include/SmoothSpline.h
    )

#список сырцов
set(${PROJECT_NAME}_C_SRCS
    SmoothSpline.c
    )

set(static_lib_c_name ${PROJECT_NAME}_static)

#собрать исполняемый файл с имянем ${PROJECT_NAME} из сырцов ${${PROJECT_NAME}_C_SRCS}
add_library(${static_lib_c_name} STATIC ${${PROJECT_NAME}_C_SRCS})

add_library(${PROJECT_NAME} SHARED ${${PROJECT_NAME}_C_SRCS})

# install
INSTALL(FILES ${${PROJECT_NAME}_C_HDRS}
    DESTINATION include/${PROJECT_NAME})
INSTALL(TARGETS ${PROJECT_NAME}
    RUNTIME DESTINATION bin
    LIBRARY DESTINATION lib
    ARCHIVE DESTINATION lib
)

set(static_lib_c_name   ${static_lib_c_name}    PARENT_SCOPE)
