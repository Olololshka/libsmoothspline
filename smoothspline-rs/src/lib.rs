#![no_std]

extern crate alloc;

mod calc_object;
mod data_point;
mod smooth_spline;
mod spline_fragment;

pub use data_point::{DataPoint, IDataPoint};
pub use smooth_spline::SmoothSpline;
pub use spline_fragment::{
    I2Differentiable, IDifferentiable, ISplineFragment, IValue, SplineFragment,
};

#[cfg(test)]
mod test {
    use super::*;

    use approx::relative_eq;
    use num_traits::Float;

    #[test]
    fn test_spline() {
        const RESULT: [(f64, f64, f64, f64, f64); 5] = [
            (
                // 0
                0.0,
                0.70710678118654757,
                0.70710678118654757,
                -0.23991629348030072,
                0.0,
            ),
            (
                // 1
                0.62831853071795862,
                0.5269199674170395,
                0.5269199674170395,
                -0.38049607578127853,
                -0.4474793450396633,
            ),
            (
                // 2
                1.2566370614359172,
                0.25358895995867409,
                0.25358895995867409,
                -0.40348749425506225,
                0.37429538706372339,
            ),
            (
                // 3
                1.8849555921538759,
                0.068932011619549649,
                0.068932011619549649,
                -0.19228535845016193,
                0.29798189103458994,
            ),
            (
                // 4
                2.5132741228718345,
                -0.012671597171099822,
                -0.012671597171099822,
                -0.098671586472455691,
                0.0,
            ),
        ];

        let mut data = [DataPoint::zero(); RESULT.len()];
        generate_data(&mut data);

        let mut spline = SmoothSpline::<_, _, SplineFragment<_>>::new(&data);
        spline.update(0.0);

        for i in 0..RESULT.len() {
            let p = data[i];
            let interpolated = spline.y(p.x);
            let d1y = spline.dy(p.x);
            let d2y = spline.d2y(p.x);
            assert!(relative_eq!(p.x, RESULT[i].0, epsilon = 1e-4));
            assert!(relative_eq!(p.y, RESULT[i].1, epsilon = 1e-4));
            assert!(relative_eq!(interpolated, RESULT[i].2, epsilon = 1e-4));
            assert!(relative_eq!(d1y, RESULT[i].3, epsilon = 1e-4));
            assert!(relative_eq!(d2y, RESULT[i].4, epsilon = 1e-4));
        }
    }

    pub(crate) fn generate_data(data: &mut [DataPoint<f64>]) {
        let len = data.len();
        for i in 0..len {
            let x = core::f64::consts::PI * (i as f64) / (len as f64);
            data[i].x = x;
            data[i].y = (-x).exp() * (x + core::f64::consts::PI / 4.0).sin();
        }
    }

    #[test]
    fn test_empty_spline() {
        let data = [DataPoint::zero(); 0];
        let mut spline = SmoothSpline::<_, _, SplineFragment<_>>::new(&data);

        assert!(spline.y(0.0).is_nan());
        assert!(spline.dy(0.0).is_nan());
        assert!(spline.d2y(0.0).is_nan());
    }

    #[test]
    fn test_invalid_spline() {
        let data = [DataPoint::<f64>::zero(); 2];
        let mut spline = SmoothSpline::<_, _, SplineFragment<_>>::new(&data);

        assert!(spline.y(1.0).is_nan());
    }

    #[test]
    fn test_calc() {
        use crate::calc_object::CalcObject;

        let mut data = [DataPoint::zero(); 20];
        generate_data(&mut data);

        let mut calc_obj = CalcObject::new(0.0, data.len());
        calc_obj.prepare(&data);
        calc_obj.calc_st1(&data);
        calc_obj.solve(&data);
    }

    #[test]
    fn test_real_data() {
        let mut data = [DataPoint::zero(); 20];
        generate_data(&mut data);

        let mut spline = SmoothSpline::<_, _, SplineFragment<_>>::new(&data);
        spline.update(0.0);

        for p in data {
            let x = p.x;
            let y = p.y;
            let yres = spline.y(x);
            assert!(yres - y < 1e-4);
        }
    }
}
