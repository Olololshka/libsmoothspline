use alloc::vec::Vec;
use num_traits::FromPrimitive;

use crate::{
    calc_object::CalcObject,
    data_point::IDataPoint,
    spline_fragment::{ISplineFragment, PointInFragment},
};

pub struct SmoothSpline<T: num_traits::Float, U: IDataPoint<T>, F: ISplineFragment<T>> {
    last_fragment: Option<F>, // cache
    spline_fragments: Vec<F>,
    points: Vec<U>,
    _phantom: core::marker::PhantomData<T>,
}

impl<T, U, F> SmoothSpline<T, U, F>
where
    T: num_traits::Float + FromPrimitive,
    U: IDataPoint<T> + Clone,
    F: ISplineFragment<T>,
{
    pub fn new(points: &[U]) -> Self {
        Self {
            last_fragment: None,
            spline_fragments: Vec::new(),
            points: points.to_vec(),
            _phantom: core::marker::PhantomData,
        }
    }

    pub fn update(&mut self, pg: T) {
        let n_win = self.points.len();
        if n_win == 0 {
            self.spline_fragments.clear();
            return;
        }
        let mut calc_obj = CalcObject::new(pg, n_win);

        calc_obj.prepare(&self.points);
        calc_obj.calc_st1(&self.points);
        calc_obj.solve(&self.points);

        self.create_fragments(calc_obj);
    }

    fn create_fragments(&mut self, calc_obj: CalcObject<T>) {
        let two = unsafe { T::from_f64(2.0).unwrap_unchecked() };
        let six = unsafe { T::from_f64(6.0).unwrap_unchecked() };

        let mut spline_fragments: Vec<F> = Vec::with_capacity(calc_obj.nwin);
        spline_fragments.resize_with(calc_obj.nwin, F::zero);

        for i in 1..=calc_obj.last_n() {
            let res_index = i - 1;
            let hi = self.points[i].x() - self.points[i - 1].x();

            if res_index == 0 {
                spline_fragments[0].set_x_min(self.points[0].x());
            } else {
                let prev_x = spline_fragments[res_index - 1].x_max();
                spline_fragments[res_index].set_x_min(prev_x);
            }

            let new_fragment = &mut spline_fragments[res_index];
            new_fragment.set_x_max(self.points[i].x());

            // set spline coefficients
            new_fragment
                .set_a((calc_obj.cm[i] - calc_obj.cm[i - 1]) / (six * hi))
                .set_b(calc_obj.cm[i - 1] / two)
                .set_c(
                    (calc_obj.ym[i] - calc_obj.ym[i - 1]) / hi
                        - (two * calc_obj.cm[i - 1] + calc_obj.cm[i]) * hi / six,
                )
                .set_d(calc_obj.ym[i - 1]);
        }
        self.spline_fragments = spline_fragments;
    }

    fn try_get_chaced_fragment(&self, x: T) -> Option<F> {
        if let Some(last_fragment) = self.last_fragment {
            if last_fragment.is_x_in(x) == PointInFragment::In {
                return Some(last_fragment);
            }
        }
        return None;
    }

    pub fn find_fragment(&mut self, x: T) -> Option<F> {
        if let Some(fragment) = self.try_get_chaced_fragment(x) {
            return Some(fragment);
        }

        if self.spline_fragments.is_empty() {
            return None;
        }

        let mut start_element = 0;
        let mut end_element = self.spline_fragments.len() - 1;
        let mut middle = end_element / 2;
        while start_element < end_element {
            let fragment = &self.spline_fragments[middle];
            let res = fragment.is_x_in(x);
            if res == PointInFragment::In {
                self.last_fragment.replace(*fragment);
                return Some(*fragment);
            } else if res == PointInFragment::Over {
                start_element = middle + 1;
            } else if middle == 0 {
                break;
            } else {
                end_element = middle - 1;
            }

            middle = (start_element + end_element) / 2;
        }

        let middle = &self.spline_fragments[middle];
        if middle.is_x_in(x) == PointInFragment::In {
            self.last_fragment.replace(*middle);
            Some(*middle)
        } else {
            None
        }
    }

    pub fn y(&mut self, x: T) -> T {
        match self.find_fragment(x) {
            Some(f) => f.y(x),
            None => T::nan(),
        }
    }

    pub fn dy(&mut self, x: T) -> T {
        match self.find_fragment(x) {
            Some(f) => f.dy(x),
            None => T::nan(),
        }
    }

    pub fn d2y(&mut self, x: T) -> T {
        match self.find_fragment(x) {
            Some(f) => f.d2y(x),
            None => T::nan(),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::{DataPoint, SplineFragment};

    use super::super::test::generate_data;
    use super::*;    

    #[test]
    fn test_find_fragment() {
        let mut data = [DataPoint::zero(); 5];
        generate_data(&mut data);

        let mut spline = SmoothSpline::<_, _, SplineFragment<_>>::new(&data);
        spline.update(0.0);

        assert!(spline.find_fragment(0.0).is_some());
        assert!(spline.find_fragment(-0.1).is_none());
        assert!(spline.find_fragment(10.0).is_none());
    }
}