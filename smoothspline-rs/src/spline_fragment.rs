use core::cmp::Ordering;

use num_traits::cast::FromPrimitive;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum PointInFragment {
    Less,
    In,
    Over,
}

impl Into<Ordering> for PointInFragment {
    fn into(self) -> Ordering {
        match self {
            PointInFragment::Less => Ordering::Greater,
            PointInFragment::In => Ordering::Equal,
            PointInFragment::Over => Ordering::Less,
        }
    }
}

pub trait IValue<T> {
    fn y(&self, x: T) -> T;
}

pub trait IDifferentiable<T>: IValue<T> {
    fn dy(&self, x: T) -> T;
}

pub trait I2Differentiable<T>: IDifferentiable<T> {
    fn d2y(&self, x: T) -> T;
}

pub trait ISplineFragment<T>: I2Differentiable<T> + Copy {
    fn is_x_in(&self, x: T) -> PointInFragment;
    fn zero() -> Self;

    fn set_x_min(&mut self, x_min: T) -> &mut Self;
    fn set_x_max(&mut self, x_max: T) -> &mut Self;
    fn set_a(&mut self, a: T) -> &mut Self;
    fn set_b(&mut self, b: T) -> &mut Self;
    fn set_c(&mut self, c: T) -> &mut Self;
    fn set_d(&mut self, d: T) -> &mut Self;

    fn x_min(&self) -> T;
    fn x_max(&self) -> T;
    fn a(&self) -> T;
    fn b(&self) -> T;
    fn c(&self) -> T;
    fn d(&self) -> T;
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct SplineFragment<T> {
    pub a: T,
    pub b: T,
    pub c: T,
    pub d: T,
    pub x_min: T,
    pub x_max: T,
}

#[allow(unused)]
impl<T> SplineFragment<T>
where
    T: num_traits::Zero,
    T: num_traits::Float,
{
    pub fn new(a: T, b: T, c: T, d: T, x_min: T, x_max: T) -> Self {
        Self {
            a,
            b,
            c,
            d,
            x_min,
            x_max,
        }
    }
}

impl<T> IValue<T> for SplineFragment<T>
where
    T: num_traits::Float + num_traits::NumOps<T, T> + FromPrimitive,
{
    fn y(&self, mut x: T) -> T {
        x = x - self.x_min;
        let x2 = x * x;
        return (self.a * x2 * x) + (self.b * x2) + (self.c * x) + self.d;
    }
}

impl<T> IDifferentiable<T> for SplineFragment<T>
where
    T: num_traits::Float + num_traits::NumOps<T, T> + FromPrimitive,
{
    fn dy(&self, mut x: T) -> T {
        x = x - self.x_min;
        return unsafe {
            (T::from_f64(3.0).unwrap_unchecked() * self.a * x * x)
                + (T::from_f64(2.0).unwrap_unchecked() * self.b * x)
                + self.c
        };
    }
}

impl<T> I2Differentiable<T> for SplineFragment<T>
where
    T: num_traits::Float + num_traits::NumOps<T, T> + FromPrimitive,
{
    fn d2y(&self, mut x: T) -> T {
        x = x - self.x_min;
        return unsafe {
            (T::from_f64(6.0).unwrap_unchecked() * self.a * x)
                + (T::from_f64(2.0).unwrap_unchecked() * self.b)
        };
    }
}

impl<T> ISplineFragment<T> for SplineFragment<T>
where
    T: num_traits::Float + num_traits::NumOps<T, T> + FromPrimitive,
{
    fn is_x_in(&self, x: T) -> PointInFragment {
        if x < self.x_min {
            PointInFragment::Less
        } else if x > self.x_max {
            PointInFragment::Over
        } else {
            PointInFragment::In
        }
    }

    fn zero() -> Self {
        Self::new(
            T::zero(),
            T::zero(),
            T::zero(),
            T::zero(),
            T::zero(),
            T::zero(),
        )
    }

    fn set_x_min(&mut self, x_min: T) -> &mut Self {
        self.x_min = x_min;
        self
    }

    fn set_x_max(&mut self, x_max: T) -> &mut Self {
        self.x_max = x_max;
        self
    }

    fn set_a(&mut self, a: T) -> &mut Self {
        self.a = a;
        self
    }

    fn set_b(&mut self, b: T) -> &mut Self {
        self.b = b;
        self
    }

    fn set_c(&mut self, c: T) -> &mut Self {
        self.c = c;
        self
    }

    fn set_d(&mut self, d: T) -> &mut Self {
        self.d = d;
        self
    }

    fn x_min(&self) -> T {
        self.x_min
    }

    fn x_max(&self) -> T {
        self.x_max
    }

    fn a(&self) -> T {
        self.a
    }

    fn b(&self) -> T {
        self.b
    }

    fn c(&self) -> T {
        self.c
    }

    fn d(&self) -> T {
        self.d
    }
}
