pub trait IDataPoint<T: num_traits::Float> {
    fn x(&self) -> T;
    fn y(&self) -> T;
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct DataPoint<T> {
    pub x: T,
    pub y: T,
}

impl<T> DataPoint<T> {
    pub fn new(x: T, y: T) -> Self {
        Self { x, y }
    }

    pub fn zero() -> Self
    where
        T: num_traits::Zero,
    {
        Self {
            x: T::zero(),
            y: T::zero(),
        }
    }
}

impl<T> IDataPoint<T> for DataPoint<T>
where
    T: num_traits::Float,
{
    fn x(&self) -> T {
        self.x
    }

    fn y(&self) -> T {
        self.y
    }
}
