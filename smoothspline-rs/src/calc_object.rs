use alloc::vec::Vec;

use num_traits::FromPrimitive;

use crate::data_point::IDataPoint;

pub(crate) struct CalcObject<T> {
    pub nwin: usize,

    pub pg: T,

    pub a: Vec<T>,
    pub b: Vec<T>,
    pub c: Vec<T>,
    pub d: Vec<T>,
    pub e: Vec<T>,
    pub f: Vec<T>,
    pub p: Vec<T>,
    pub q: Vec<T>,
    pub cm: Vec<T>,
    pub ym: Vec<T>,

    pub y1: T,
    pub h1: T,
    pub h2: T,

    pub yn: T,
    pub hn1: T,
    pub hn2: T,
}

impl<T> CalcObject<T>
where
    T: num_traits::Float + num_traits::NumOps<T, T> + FromPrimitive,
{
    pub fn new(pg: T, n_win: usize) -> Self {
        let mut res = Self {
            nwin: n_win,
            pg,
            a: Vec::with_capacity(n_win),
            b: Vec::with_capacity(n_win),
            c: Vec::with_capacity(n_win),
            d: Vec::with_capacity(n_win),
            e: Vec::with_capacity(n_win),
            f: Vec::with_capacity(n_win),
            p: Vec::with_capacity(n_win),
            q: Vec::with_capacity(n_win),
            cm: Vec::with_capacity(n_win),
            ym: Vec::with_capacity(n_win),
            y1: T::zero(),
            h1: T::zero(),
            h2: T::zero(),
            yn: T::zero(),
            hn1: T::zero(),
            hn2: T::zero(),
        };

        res.a.resize_with(n_win, T::zero);
        res.b.resize_with(n_win, T::zero);
        res.c.resize_with(n_win, T::zero);
        res.d.resize_with(n_win, T::zero);
        res.e.resize_with(n_win, T::zero);
        res.f.resize_with(n_win, T::zero);
        res.p.resize_with(n_win, T::zero);
        res.q.resize_with(n_win, T::zero);
        res.cm.resize_with(n_win, T::zero);
        res.ym.resize_with(n_win, T::zero);

        res
    }

    pub fn last_n(&self) -> usize {
        self.nwin - 1
    }

    pub fn n1(&self) -> usize {
        self.nwin - 2
    }

    pub fn n2(&self) -> usize {
        self.nwin - 3
    }

    pub fn n3(&self) -> usize {
        self.nwin - 4
    }

    pub fn prepare<P: IDataPoint<T>>(&mut self, points: &[P]) {
        let last_n = self.last_n();
        let n1 = self.n1();
        let n2 = self.n2();

        let one = unsafe { T::from_f64(1.0).unwrap_unchecked() };
        let two = unsafe { T::from_f64(2.0).unwrap_unchecked() };
        let three = unsafe { T::from_f64(3.0).unwrap_unchecked() };
        let six = unsafe { T::from_f64(6.0).unwrap_unchecked() };

        self.y1 = (points[1].y() - points[0].y()) / (points[1].x() - points[0].x());
        self.h1 = points[1].x() - points[0].x();
        self.h2 = points[2].x() - points[1].x();

        self.c[0] = (self.h1 / three) + (two / (self.h1 * self.h1) * self.pg);
        let hi_div6 = self.h1 / six;
        self.d[0] = hi_div6
            - (one / self.h1 * ((one / self.h1) + (one / self.h2)) * self.pg)
            - (self.pg / (self.h1 * self.h1));
        self.e[0] = self.pg / (self.h1 * self.h2);
        self.f[0] = ((points[1].y() - points[0].y()) / self.h1) - self.y1;
        self.b[1] = self.d[0];
        self.a[2] = self.e[0];

        self.yn = (points[last_n].y() - points[n1].y()) / (points[last_n].y() - points[n1].y());
        self.hn1 = points[last_n].x() - points[n1].x();
        self.hn2 = points[n1].x() - points[n2].x();

        self.a[last_n] = self.pg / (self.hn1 * self.hn2);
        self.b[last_n] = (self.hn1 / six)
            - (one / self.hn1 * ((one / self.hn1) + (one / self.hn2)) * self.pg)
            - (self.pg / (self.hn1 * self.hn1));
        self.c[last_n] = (self.hn1 / three) + (two / (self.hn1 * self.hn1) * self.pg);
        self.d[n1] = self.b[last_n];
        self.e[n2] = self.a[last_n];
        self.f[last_n] = self.yn - ((points[last_n].y() - points[n1].y()) / self.hn1);
    }

    pub fn calc_st1<P: IDataPoint<T>>(&mut self, points: &[P]) {
        let n1 = self.n1();
        let n2 = self.n2();
        let n3 = self.n3();

        let one = unsafe { T::from_f64(1.0).unwrap_unchecked() };
        let two = unsafe { T::from_f64(2.0).unwrap_unchecked() };
        let three = unsafe { T::from_f64(3.0).unwrap_unchecked() };
        let six = unsafe { T::from_f64(6.0).unwrap_unchecked() };

        for i in 1..=n1 {
            let ii = i - 1;
            let i1 = i + 1;
            let hii = points[i].x() - points[ii].x();
            let hi = points[i1].x() - points[i].x();

            self.f[i] =
                (points[i1].y() - points[i].y()) / hi - (points[i].y() - points[ii].y()) / hii;
            self.c[i] = ((hii + hi) / three)
                + (one / (hii * hii) * self.pg)
                + (((one / hii) + (one / hi)).powf(two) * self.pg)
                + (self.pg / (hi * hi));
            if i < n2 + 1 {
                let i2 = i + 2;
                let hi1 = points[i2].x() - points[i1].x();
                self.d[i] = (hi / six)
                    - (one / hi)
                        * ((one / hii + one / hi) * self.pg
                            + (((one / hi) + (one / hi1)) * self.pg));
                self.b[i1] = self.d[i];
                if i < n3 + 1 {
                    self.e[i] = self.pg / (hi * hi1);
                    self.a[i2] = self.e[i];
                }
            }
        }
    }

    pub fn solve<P: IDataPoint<T>>(&mut self, points: &[P]) {
        let last_n = self.last_n();
        let n1 = self.n1();

        self.p[1] = -self.d[1] / self.c[1];
        self.q[1] = -self.e[1] / self.c[1];
        self.cm[1] = self.f[1] / self.c[1];

        let cp = self.c[2] + (self.b[2] * self.p[1]);
        self.p[2] = -(self.d[2] + (self.b[2] * self.q[1])) / cp;
        self.q[2] = -self.e[2] / cp;
        self.cm[2] = (self.f[2] - (self.b[2] * self.cm[1])) / cp;
        self.e[n1] = T::zero();
        self.d[last_n] = T::zero();
        self.e[last_n] = T::zero();

        for i in 2..last_n {
            let i1 = i - 1;
            let i2 = i - 2;
            let cb = (self.a[i] * self.p[i2]) + self.b[i];
            let ck = self.c[i] + (cb * self.p[i1]) + (self.a[i] * self.q[i2]);
            self.p[i] = -(self.d[i] + (cb * self.q[i1])) / ck;
            self.q[i] = -self.e[i] / ck;
            self.cm[i] = (self.f[i] - (cb * self.cm[i1]) - (self.a[i] * self.cm[i2])) / ck;
        }

        self.cm[n1] = (self.p[n1] * self.cm[last_n]) + self.cm[n1];

        for i in 2..last_n {
            let k = last_n - i;
            let k1 = k + 1;
            let k2 = k + 2;
            self.cm[k] = (self.p[k] * self.cm[k1]) + (self.q[k] * self.cm[k2]) + self.cm[k];
        }

        for i in 1..=n1 {
            let ii = i - 1;
            let i1 = i + 1;
            let hii = points[i].x() - points[ii].x();
            let hi = points[i1].x() - points[i].x();

            let ym = points[i].y()
                - self.pg * (((self.cm[i1] - self.cm[i]) / hi)
                - ((self.cm[i] - self.cm[ii]) / hii));
            self.ym[i] = ym;
        }

        self.ym[0] =
            points[0].y() - (self.pg * (self.cm[1] - self.cm[0]) / (points[1].x() - points[0].x()));
        self.ym[last_n] = points[last_n].y()
            + (self.pg * (self.cm[last_n] - self.cm[n1]) / (points[last_n].x() - points[n1].x()));
    }
}
