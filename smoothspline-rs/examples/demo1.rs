use std::f64::consts::PI;

use smoothspline::{DataPoint, SmoothSpline, SplineFragment};

fn generate_data(data: &mut [DataPoint<f64>]) {
    let len = data.len();
    for i in 0..len {
        let x =  PI * (i as f64) / (len as f64);
        data[i].x = x;
        data[i].y = x.exp() * (x + PI / 4.0).sin();
    }
}

fn main() {
    const POINTS: usize = 20;
    let mut data = [DataPoint::zero(); POINTS];

    generate_data(&mut data);

    let mut spline = SmoothSpline::<_, _, SplineFragment<_>>::new(&data);
    spline.update(0.0);

    for i in 0..POINTS {
        let p = data[i];
        let interpolated = spline.y(p.x);
        let d1y = spline.dy(p.x);
        let d2y = spline.d2y(p.x);
        println!("{};{};{};{};{}", p.x, p.y, interpolated, d1y, d2y);
    }
}
