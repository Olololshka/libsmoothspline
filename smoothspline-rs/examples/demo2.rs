use std::{
    fs::File,
    io::{self, BufRead, BufReader},
};

use regex::Regex;
use smoothspline::{DataPoint, SmoothSpline, SplineFragment};

lazy_static::lazy_static! {
    // 14.07.2017 09:20:57.538
    static ref RE: Regex = Regex::new(r"(\d+)\.(\d+)\.(\d+) (\d+):(\d+):(\d+)\.(\d+)").unwrap();
}

fn date2seconds(date: &str) -> Option<f64> {
    RE.captures(date).map(|captures| {
        let day = captures.get(1).unwrap().as_str().parse::<u32>().unwrap();
        let month = captures.get(2).unwrap().as_str().parse::<u32>().unwrap();
        let year = captures.get(3).unwrap().as_str().parse::<u32>().unwrap();
        let hour = captures.get(4).unwrap().as_str().parse::<u32>().unwrap();
        let minute = captures.get(5).unwrap().as_str().parse::<u32>().unwrap();
        let second = captures.get(6).unwrap().as_str().parse::<u32>().unwrap();
        let millisecond = captures.get(7).unwrap().as_str().parse::<u32>().unwrap();

        let date = chrono::NaiveDate::from_ymd_opt(year as i32, month, day);
        let time = chrono::NaiveTime::from_hms_milli_opt(hour, minute, second, millisecond);

        if date.is_none() || time.is_none() {
            return 0.0;
        }

        let datetime = chrono::NaiveDateTime::new(date.unwrap(), time.unwrap());

        let datetime = datetime.timestamp_millis();

        datetime as f64 / 1000.0
    })
}

fn main() -> io::Result<()> {
    if std::env::args().len() < 2 {
        return Err(io::Error::new(
            io::ErrorKind::Other,
            format!(
                "Usage: {} <datafile> [smoothfactor > 0]",
                std::env::args().nth(0).unwrap()
            ),
        ));
    }
    let mut smoothfactor = 0.0;
    
    let file = File::open(std::env::args().nth(1).unwrap())?;
    let reader = BufReader::new(file);

    // print filename to stderr
    eprintln!("{}", std::env::args().nth(1).unwrap());

    if let Some(s) = std::env::args().nth(2) {
        smoothfactor = s.parse::<f64>().unwrap();
        eprintln!("smoothfactor: {}", smoothfactor);
    }

    let mut data = Vec::new();

    let mut prev_date: f64 = f64::NAN;
    for line in reader.lines() {
        match line {
            Ok(line) => {
                let mut split = line.split(";");

                if let Some(date) = date2seconds(split.next().unwrap()) {
                    if date == prev_date {
                        continue;
                    }
                    prev_date = date;

                    let value = split
                        .next()
                        .unwrap()
                        .replace(",", ".")
                        .parse::<f64>()
                        .unwrap();

                    let data_point = DataPoint::<f64>::new(date, value);
                    data.push(data_point);
                }
            }
            Err(e) => Err(e)?,
        }
    }

    let mut spline = SmoothSpline::<_, _, SplineFragment<_>>::new(&data);
    spline.update(smoothfactor);

    for i in 0..data.len() {
        let v = data[i].x;
        let origin = data[i].y;
        let res = spline.y(v);
        let d1y = spline.dy(v);
        let d2y = spline.d2y(v);
        println!("{};{};{};{};{}", v, origin, res, d1y, d2y);
    }

    Ok(())
}
