#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <regex>
#include <math.h>
#include <list>

#include "SmoothSpline.h"

void generateData(double *pX, double *pY, int points)
{
    for (int i = 0; i < points; ++i) {
        pX[i] = M_PI / points * i;
        pY[i] = exp(pX[i]) * sin(pX[i] + M_PI / 4.0);
    }
}

int main(int argc, char* argv[]) {
    const int points = 20;
    double *mX = new double[points];
    double *mY = new double[points];
    generateData(mX, mY, points);

    spline_range *spline;

    SplineApprox(mX, mY, points, 0, &spline);

    int spline_size = points - 1;

    for (int i = 0; i < points; ++i) {
        auto v = mX[i];
        auto origin = mY[i];
        auto res = calculateSplineAt(spline, spline_size, v);
        printf("%f;%f;%f;%f;%f\n", v, origin, res.Y, res.d1Y, res.d2Y);
    }

    return 0;
}
