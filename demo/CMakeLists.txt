cmake_minimum_required(VERSION 3.1)


set (CMAKE_CXX_STANDARD 11)

SET(demo1_src demo1.cpp)
SET(demo2_src demo2.cpp)
SET(demo3_src demo_cpp1.cpp)

include_directories(
    ../src/include
    ../src_cpp/include
    )

add_executable(demo1 ${demo1_src})
target_link_libraries(demo1 ${static_lib_c_name})

add_executable(demo2 ${demo2_src})
target_link_libraries(demo2 ${static_lib_c_name})

add_executable(demo_cpp1 ${demo3_src})
