#ifndef _TEST_SMOOTHSPLINE_CC_H_
#define _TEST_SMOOTHSPLINE_CC_H_

#include <chrono>
#include <functional>
#include <random>
#include <thread>
#include <vector>

#include <SmoothSpline++.h>

#include <cxxtest/TestSuite.h>

class TestSmothSplinr_pp : public CxxTest::TestSuite {
public:
  void testCopy() {
    smsp::SmoothSpline<double> spline(genRandomData());
    smsp::SmoothSpline<double> sp2(spline);
  }

  void testCopyAssign() {
    smsp::SmoothSpline<double> spline(genRandomData()), sp2;
    sp2 = spline;
  }

  void testMove() {
    smsp::SmoothSpline<double> spline(genRandomData());
    smsp::SmoothSpline<double> sp2(std::move(spline));
  }

  void testMoveAssign() {
    smsp::SmoothSpline<double> spline(genRandomData()), sp2;
    sp2 = std::move(spline);
  }

  void testFromVectorPairs() {
    smsp::SmoothSpline<double> s1({smsp::Point<double>{0.0, 0.0}}),
        s2({std::pair<float, double>{0.0, 0.0}});
  }

  void testArgumentIncrease() {
    auto data = genData([](double X) { return 10 * sin(X); }, 20);

    smsp::SmoothSpline<double> s(data);

    auto &f_start = s.find_fragment(data.cbegin()->X);
    auto &f_end = s.find_fragment((data.cend() - 1)->X);

    TS_ASSERT(f_start.Xmin() < f_start.Xmax());
    TS_ASSERT(f_end.Xmin() < f_end.Xmax());
  }

  void testArgumentdecrease() {
    auto data = genData([](double X) { return 10 * sin(X); }, 20);
    std::vector<smsp::Point<double>> rev_data(data.size());
    std::reverse_copy(data.cbegin(), data.cend(), rev_data.begin());

    smsp::SmoothSpline<double> s(rev_data);

    auto &f_start = s.find_fragment(rev_data.cbegin()->X);
    auto &f_end = s.find_fragment((rev_data.cend() - 1)->X);

    TS_ASSERT(f_start.Xmin() < f_start.Xmax());
    TS_ASSERT(f_end.Xmin() < f_end.Xmax());
  }

  void testWorkWithItherators() {
    auto data = genData([](double X) { return 10 * X + 1; }, 20);
    smsp::SmoothSpline<double> s;
    s.Update(data.cbegin(), data.cend());

    auto &f_start = s.find_fragment(data.cbegin()->X);
    auto &f_end = s.find_fragment((data.cend() - 1)->X);

    TS_ASSERT(f_start.Xmin() < f_start.Xmax());
    TS_ASSERT(f_end.Xmin() < f_end.Xmax());
  }

  void testWithListItherators() {
    auto data = genData([](double X) { return 10 * X + 1; }, 20);
    std::list<smsp::Point<double>> list(data.size());
    smsp::SmoothSpline<double> s;

    std::copy(data.cbegin(), data.cend(), list.begin());

    s.Update(list.cbegin(), list.cend());

    auto &f_start = s.find_fragment(list.cbegin()->X);
    auto &f_end = s.find_fragment(list.crbegin()->X);

    TS_ASSERT(f_start.Xmin() < f_start.Xmax());
    TS_ASSERT(f_end.Xmin() < f_end.Xmax());
  }

  static std::vector<smsp::Point<double>>
  genData(const std::function<double(double)> &f, size_t count = 50) {
    std::vector<smsp::Point<double>> res(count);

    auto step = 1.0 / count;
    for (auto pos = 0; pos < count; ++pos) {
      auto p = res.begin() + pos;
      p->X = pos * step;
      p->Y = f(p->X);
    }
    return res;
  }

  static std::vector<smsp::Point<double>> genRandomData(size_t count = 50) {
    std::vector<smsp::Point<double>> res(count);

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<float> dis(-1.0, 1.0);

    std::for_each(res.begin(), res.end(), [&dis, &gen](smsp::Point<double> &p) {
      p.X = std::chrono::milliseconds(std::chrono::seconds(std::time(NULL)))
                .count();
      p.Y = dis(gen);
    });
    return res;
  }
};

#endif /* _TEST_SMOOTHSPLINE_CC_H_ */
