

from unittest import TestCase
from libSmoothSpline import SmoothSpline, Point, SplineFragment
import math


def generateData(points):
    res = []
    for i in range(points):
        r = Point()
        r.X = math.pi / points * i
        r.Y = math.exp(-r.X) * math.sin(r.X + math.pi / 4)
        res.append(r)

    return res


class TestSpline(TestCase):
    def test_empty_spline(self):
        s = SmoothSpline()
        self.assertTrue(math.isnan(s.Y(0)))
        self.assertTrue(math.isnan(s.dY(0)))
        self.assertTrue(math.isnan(s.d2Y(0)))

    def test_invalid_spline(self):
        s = SmoothSpline()
        sf = SplineFragment()
        s._spline_fragments.append(sf)
        s._spline_fragments.append(sf)
        self.assertTrue(math.isnan(s.Y(1)))

    def test_calc_stage1(self):
        s = SmoothSpline()
        s.Points = generateData(20)

        A1, B1, C1, D1, E1, F1, P1, Q1, CM1, ym1, lastN1, N11, N21, N31 = s._prepareCalc(0)
        A2, B2, C2, D2, E2, F2, P2, Q2, CM2, ym2, lastN2, N12, N22, N32 = s._prepareCalc(0)

        s.Calc_old(A1, B1, C1, D1, E1, F1, N11, N21, N31, 0)
        s._calc_new(A2, B2, C2, D2, E2, F2, N12, N22, N32, 0)

        self.assertEqual(A1, A2)
        self.assertEqual(B1, B2)
        self.assertEqual(C1, C2)
        self.assertEqual(D1, D2)
        self.assertEqual(E1, E2)
        self.assertEqual(F1, F2)


    def test_solve(self):
        s = SmoothSpline()
        s.Points = generateData(20)

        A1, B1, C1, D1, E1, F1, P1, Q1, CM1, ym1, lastN1, N11, N21, N31 = s._prepareCalc(0)
        A2, B2, C2, D2, E2, F2, P2, Q2, CM2, ym2, lastN2, N12, N22, N32 = s._prepareCalc(0)

        s.Calc_old(A1, B1, C1, D1, E1, F1, N11, N21, N31, 0)
        s._calc_new(A2, B2, C2, D2, E2, F2, N12, N22, N32, 0)

        s._solve(A1, B1, C1, D1, E1, F1, P1, Q1, CM1, ym1, N11, lastN1, 0)
        s._solve(A2, B2, C2, D2, E2, F2, P2, Q2, CM2, ym2, N12, lastN2, 0)

        self.assertEqual(A1, A2)
        self.assertEqual(B1, B2)
        self.assertEqual(C1, C2)
        self.assertEqual(D1, D2)
        self.assertEqual(E1, E2)
        self.assertEqual(F1, F2)
        self.assertEqual(P1, P2)
        self.assertEqual(Q1, Q2)
        self.assertEqual(CM1, CM2)
        self.assertEqual(ym1, ym2)

    def test_real_data(self):
        s = SmoothSpline()
        s.Points = generateData(20)
        s.Update(0)

        for p in s.Points:
            x = p.X
            y = p.Y
            yres = s.Y(x)
            assert abs(y - yres) < 1e-4, "x = {}, y = {}, yres = {}".format(x, y, yres)