#!/usr/bin/python
# -*- codding: utf-8 -*-


from _ast import Expression

from libSmoothSpline import SmoothSpline, Point
import sys
import re
from datetime import datetime


epoch = datetime.utcfromtimestamp(0)
parcer = re.compile("(\\d+)\\.(\\d+)\\.(\\d+) (\\d+):(\\d+):(\\d+)\\.(\\d+)")

def parce_date(string):
    match = parcer.match(string)
    if not match:
        raise Expression()

    dt = datetime(year=int(match.group(3)),
                 month=int(match.group(2)),
                 day=int(match.group(1)),
                 hour=int(match.group(4)),
                 minute=int(match.group(5)),
                 second=int(match.group(6)),
                 microsecond=int(match.group(7)) * 1000)
    return (dt - epoch).total_seconds()


def main():
    if (len(sys.argv) == 1):
        print('Usage: {} <datafile> [smoothfactor > 0]'.format(sys.argv[0]))
        return 1

    smoothfactor = 0.0

    if (len(sys.argv) >= 3):
        smoothfactor = float(sys.argv[2])

    points = []

    with open(sys.argv[1], mode='r') as ins:
        for line in ins:
            sep = line.split(';')

            try:
                val = float(sep[1].replace(',', '.'))
            except:
                continue
            date = parce_date(sep[0])

            points.append(Point(date, val))

    spline = SmoothSpline()
    spline.Points = points
    spline.Update(smoothfactor)

    for p in spline.Points:
        x = p.X
        y = p.Y
        Y = spline.Y(x)
        d1Y = spline.dY(x)
        d2Y = spline.d2Y(x)

        print('{};{};{};{};{}'.format(x, y, Y, d1Y, d2Y))


# чтобы при импорте не выполнялся код автоматом
if __name__ == '__main__':
    main()
