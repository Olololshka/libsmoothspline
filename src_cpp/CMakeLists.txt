cmake_minimum_required(VERSION 3.1)


set (CMAKE_CXX_STANDARD 11)

include_directories(include)

#список хедеров проекта (не Q_OBJECT'ов!)
set(${PROJECT_NAME}_CPP_HDRS
    include/SmoothSpline++.h
    include/SplineFragment.h
    )

add_custom_target(cpp_h SOURCES ${${PROJECT_NAME}_CPP_HDRS})

# генерация документации
#SET(DOXY_OUTPUT_LANGUAGE "Russian")
#SET(DOXY_INPUT ${PROJECT_SOURCE_DIR})

#SET(DOXY_ENABLED_SECTIONS "user_sec")
#SET(DOXY_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/doc-user")
#ADD_DOCUMENTATION(user_doc Doxyfile)

#SET(DOXY_ENABLED_SECTIONS "developer_sec")
#SET(DOXY_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/doc-developer")
#ADD_DOCUMENTATION(developer_doc Doxyfile)

#ADD_CUSTOM_TARGET(doc DEPENDS user_doc developer_doc)

# install
INSTALL(FILES ${${PROJECT_NAME}_CPP_HDRS}
    DESTINATION include/${PROJECT_NAME}++)
