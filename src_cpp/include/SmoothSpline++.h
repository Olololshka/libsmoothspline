#ifndef _SMOOTH_SPLINE_PP_H_
#define _SMOOTH_SPLINE_PP_H_

#include <algorithm>
#include <list>
#include <memory>
#include <vector>

#include <SplineFragment.h>

/** define macro SMOOTH_SPLINE_MINIMAL before include this file
 * to discart use debug info at all
 */

namespace smsp {

template <typename Real> struct Point {
  constexpr Point(Real x = 0, Real y = 0) : X(x), Y(y) {}

  template <typename T, typename U>
  constexpr Point(const std::pair<T, U> &point)
      : X(static_cast<Real>(point.first)), Y(static_cast<Real>(point.second)) {}

  Real X, Y;
};

template <typename IT, typename Criteria>
IT binary_search(IT first, IT last, const Criteria &c) {
  const IT _end(last);
  while (first < last) {
    const IT middle(first + std::distance(first, last) / 2);
    const auto comp_res = c(*middle);

    if (!static_cast<bool>(comp_res)) {
      return middle;
    } else if (static_cast<int>(comp_res) > 0) {
      first = middle + 1;
    } else {
      last = middle - 1;
    }
  }

  if (last == _end)
    return _end;
  return !static_cast<bool>(c(*last)) ? last : _end;
}

#ifdef SMOOTH_SPLINE_MINIMAL
template <typename Real>
#else
template <typename Real, bool debug = false>
#endif
class SmoothSpline {
  static_assert(std::is_floating_point<Real>::value,
                "Template argument mast be floating-point type");

  using point = Point<Real>;

  std::vector<SplineFragment<Real>> spline_fragments;
  ISplineFragment<Real> const *last_fragment; // cache last fragment
  const EmptySplineFragment<Real> emptyFragment;
  bool x_increasing;

  struct Calc_object {
    const int NWin;

    // store all temporary variables in single memory array
    std::vector<Real> arrays;
    Real *A, *B, *C, *D, *E, *F, *P, *Q, *CM, *ym;

    int lastN, N1, N2, N3;

    Real y1, h1, h2;
    Real yn, hn1, hn2;

    Real pg;

    Calc_object(int NWin, Real pg = 0)
        : NWin(NWin), lastN(NWin - 1), N1(NWin - 2), N2(NWin - 3), N3(NWin - 4),
          arrays(NWin * 10), pg(pg) {
      auto p = arrays.data();
      A = p;
      B = A + NWin;
      C = B + NWin;
      D = C + NWin;
      E = D + NWin;
      F = E + NWin;
      P = F + NWin;
      Q = P + NWin;
      CM = Q + NWin;
      ym = CM + NWin;
    }

    template <typename IT> void prepare(IT &&Points_start, IT &&Points_end) {
      const auto it_P0 = std::next(Points_start);
      const auto it_P1 = std::next(it_P0);
      const auto it_P2 = std::next(it_P1);

      const auto it_PlastN = std::prev(Points_end);
      const auto it_PN1 = std::prev(it_PlastN);
      const auto it_PN2 = std::prev(it_PN1);

      y1 = (it_P1->Y - it_P0->Y) / (it_P1->X - it_P0->X);
      h1 = it_P1->X - it_P0->X;
      h2 = it_P2->X - it_P1->X;

      C[0] = h1 / 3.0 + 2.0 / (h1 * h1) * pg;
      D[0] = h1 / 6.0 - 1.0 / h1 * (1.0 / h1 + 1.0 / h2) * pg - pg / (h1 * h1);
      E[0] = pg / (h1 * h2);
      F[0] = (it_P1->Y - it_P0->Y) / h1 - y1;
      B[1] = D[0];
      A[2] = E[0];

      yn = (it_PlastN->Y - it_PN1->Y) / (it_PlastN->X - it_PN1->X);
      hn1 = it_PlastN->X - it_PN1->X;
      hn2 = it_PN1->X - it_PN2->X;

      A[lastN] = pg / (hn1 * hn2);
      B[lastN] = hn1 / 6.0 - 1.0 / hn1 * (1.0 / hn1 + 1.0 / hn2) * pg -
                 pg / (hn1 * hn1);
      C[lastN] = hn1 / 3.0 + 2.0 / (hn1 * hn1) * pg;
      D[N1] = B[lastN];
      E[N2] = A[lastN];
      F[lastN] = yn - (it_PlastN->Y - it_PN1->Y) / hn1;
    }

    template <typename IT> void Calc_st1(IT &&Points_start) {
      auto P_i = std::next(Points_start);
      auto P_ii = Points_start;
      auto P_i1 = std::next(P_i);

      for (size_t i = 1; i <= N1; ++i, P_i++, P_ii++, P_i1++) {
        const auto i1 = i + 1;

        const auto hii = P_i->X - P_ii->X;
        const auto hi = P_i1->X - P_i->X;

        F[i] = (P_i1->Y - P_i->Y) / hi - (P_i->Y - P_ii->Y) / hii;
        C[i] = (hii + hi) / 3 + 1 / (hii * hii) * pg +
               (1 / hii + 1 / hi) * (1 / hii + 1 / hi) * pg + pg / (hi * hi);
        if (i < N2 + 1) {
          const auto i2 = i + 2;
          const auto P_i2 = std::next(P_i1);
          const auto hi1 = P_i2->X - P_i1->X;

          D[i] = hi / 6.0 - 1.0 / hi *
                                ((1.0 / hii + 1.0 / hi) * pg +
                                 (1.0 / hi + 1.0 / hi1) * pg);
          B[i1] = D[i];
          if (i < N3 + 1) {
            E[i] = pg / (hi * hi1);
            A[i2] = E[i];
          }
        }
      }
    }

    template <typename IT> void solve(IT &&Points_start, IT &&Points_end) {
      P[1] = -D[1] / C[1];
      Q[1] = -E[1] / C[1];
      CM[1] = F[1] / C[1];

      const auto CP = C[2] + B[2] * P[1];
      P[2] = -(D[2] + B[2] * Q[1]) / CP;
      Q[2] = -E[2] / CP;
      CM[2] = (F[2] - B[2] * CM[1]) / CP;
      E[N1] = 0;
      D[lastN] = 0;
      E[lastN] = 0;

      for (size_t i = 2; i < lastN; ++i) {
        const auto i1 = i - 1;
        const auto i2 = i - 2;
        const auto CB = A[i] * P[i2] + B[i];
        const auto CK = C[i] + CB * P[i1] + A[i] * Q[i2];
        P[i] = -(D[i] + CB * Q[i1]) / CK;
        Q[i] = -E[i] / CK;
        CM[i] = (F[i] - CB * CM[i1] - A[i] * CM[i2]) / CK;
      }

      CM[N1] = P[N1] * CM[lastN] + CM[N1];

      for (size_t i = 2; i < lastN; ++i) {
        const auto k = lastN - i;
        const auto k1 = k + 1;
        const auto k2 = k + 2;
        CM[k] = P[k] * CM[k1] + Q[k] * CM[k2] + CM[k];
      }

      {
        auto P_i = std::next(Points_start);
        auto P_ii = Points_start;
        auto P_i1 = std::next(P_i);

        for (size_t i = 1; i <= N1; ++i, P_i++, P_ii++, P_i1++) {
          const auto ii = i - 1;
          const auto i1 = i + 1;

          const auto hii = P_i->X - P_ii->X;
          const auto hi = P_i1->X - P_i->X;
          ym[i] =
              P_i->Y - pg * ((CM[i1] - CM[i]) / hi - (CM[i] - CM[ii]) / hii);
        }
      }

      {
        const auto P0 = Points_start;
        const auto P1 = std::next(Points_start);
        const auto P_lastN = std::prev(Points_end);
        const auto P_N1 = std::prev(P_lastN);

        ym[0] = P0->Y - pg * (CM[1] - CM[0]) / (P1->X - P0->X);
        ym[lastN] =
            P_lastN->Y + pg * (CM[lastN] - CM[N1]) / (P_lastN->X - P_N1->X);
      }
    }
  };

  template <typename T> struct delete_pointer_element {
    void operator()(T element) const { delete element; }
  };

  void clear_fragments() { spline_fragments.clear(); }

  template <typename IT>
  void createFragments(IT &&begin, IT &&end, const Calc_object &data) {
    clear_fragments();
    spline_fragments.assign(data.NWin - 1, SplineFragment<Real>());

    {
      auto last = end;
      last--;
      x_increasing = begin->X <= last->X;
    }

    auto begin_i = begin;
    begin_i++; // can't use begin + 1 witn list iteratos
    auto begin_im1 = begin;
    for (int i = 1; i <= data.lastN; ++i, ++begin_i, ++begin_im1) {
      const auto res_index = i - 1;
      const auto point_i_X = begin_i->X;
      const auto point_res_index_X = begin_im1->X;
      const auto hi = point_i_X - point_res_index_X;

      auto min = point_i_X;
      if (x_increasing) {
        min =
            (res_index > 0) ? spline_fragments[res_index - 1].Xmax() : begin->X;
      }
      auto max = x_increasing ? point_i_X : point_res_index_X;

      spline_fragments[res_index] = std::move(SplineFragment<Real>{
          (data.CM[i] - data.CM[res_index]) / (6 * hi), data.CM[res_index] / 2,
          (data.ym[i] - data.ym[res_index]) / hi -
              (2 * data.CM[res_index] + data.CM[i]) * hi / 6,
          data.ym[res_index], min, max});
    }
  }

  void clone_from(const SmoothSpline &lh) {
    spline_fragments.assign(lh.spline_fragments.cbegin(),
                            lh.spline_fragments.cend());

    if (lh.last_fragment) {
      // copy pointer against new vector
      last_fragment =
          spline_fragments.data() +
          (static_cast<const SplineFragment<Real> *>(lh.last_fragment) -
           lh.spline_fragments.data());
    }
  }

public:
  SmoothSpline() : last_fragment(nullptr) {}

  explicit SmoothSpline(const std::vector<point> &points, const Real pg = 0)
      : last_fragment(nullptr) {
    Update(points, pg);
  }

  explicit SmoothSpline(const std::list<point> &points, const Real pg = 0)
      : last_fragment(nullptr) {
    Update(points.begin(), points.end(), pg);
  }

  SmoothSpline(std::initializer_list<point> points, const Real pg = 0)
      : last_fragment(nullptr) {
    std::vector<point> tp(points);
    Update(tp.begin(), tp.end(), pg);
  }

  SmoothSpline(const SmoothSpline &lh)
      : spline_fragments(lh.spline_fragments.size()) {
    clone_from(lh);
  }

  SmoothSpline &operator=(const SmoothSpline &lh) {
    clone_from(lh);
    return *this;
  }

  SmoothSpline(SmoothSpline &&) = default;
  SmoothSpline &operator=(SmoothSpline &&) = default;

  ~SmoothSpline() { clear_fragments(); }

  void Update(const std::vector<point> &points, const Real pg = 0) {
    Update(points.begin(), points.end());
  }

  template <typename IT> void Update(IT &&begin, IT &&end, const Real pg = 0) {
    auto Nwin = std::distance(begin, end);
    if (Nwin == 0) {
      spline_fragments.clear();
      return;
    }

    Calc_object data(Nwin, pg);

    data.prepare(std::forward<IT>(begin), std::forward<IT>(end));
    data.Calc_st1(std::forward<IT>(begin));
    data.solve(std::forward<IT>(begin), std::forward<IT>(end));

    createFragments(std::forward<IT>(begin), std::forward<IT>(end), data);
  }

  const ISplineFragment<Real> &find_fragment(const Real x) {
    if (last_fragment && last_fragment->isXin(x) == PointInFragment::IN)
      return *last_fragment;

    const bool x_increasing = this->x_increasing;

#ifndef SMOOTH_SPLINE_MINIMAL
    if (debug) {
      printf("\n=> Serching fragment what contains X = %0.9f\n", x);
    }
#endif
    auto res = smsp::binary_search(
        spline_fragments.cbegin(), spline_fragments.cend(),
        [x, x_increasing](const ISplineFragment<Real> &fragment) {
          auto cr = static_cast<int>(fragment.isXin(x));
#ifndef SMOOTH_SPLINE_MINIMAL
          if (debug) {
            printf("checking: %s result: %c\n", fragment.toString().c_str(),
                   (cr > 0) ? '>' : ((cr < 0) ? '<' : '='));
          }
#endif
          return x_increasing ? cr : -cr;
        });

    if (res != spline_fragments.cend()) {
#ifndef SMOOTH_SPLINE_MINIMAL
      if (debug) {
        printf("fragment found (#%ld of %lu)\n",
               res - spline_fragments.cbegin() + 1, spline_fragments.size());
      }
#endif
      last_fragment = &(*res); // iterator -> pointer
      return *last_fragment;
    } else {
#ifndef SMOOTH_SPLINE_MINIMAL
      if (debug) {
        printf("fragment not found!\n");
      }
#endif
      last_fragment = &emptyFragment;
      return emptyFragment;
    }
  }

  Real Y(const Real X) { return find_fragment(X).Y(X); }
  Real dY(const Real X) { return find_fragment(X).dY(X); }
  Real d2Y(const Real X) { return find_fragment(X).d2Y(X); }
}; // namespace smsp

} // namespace smsp

#endif /* _SMOOTH_SPLINE_PP_H_ */
