﻿#ifndef _SPLINE_FRAGMENT_
#define _SPLINE_FRAGMENT_

#include <iomanip>
#include <limits>
#include <memory>
#include <sstream>

/** define macro SMOOTH_SPLINE_MINIMAL before include this file
 * to discart use debug info at all
 */

namespace smsp {

enum class PointInFragment { LESS = -1, IN = 0, OVER = 1 };

template <typename Real> struct ISplineFragment {
  virtual Real Y(Real x) const = 0;
  virtual Real dY(Real x) const = 0;
  virtual Real d2Y(Real x) const = 0;
  virtual Real Xmin() const = 0;
  virtual Real Xmax() const = 0;
  virtual PointInFragment isXin(Real x) const = 0;
  virtual ~ISplineFragment() {}
  virtual std::unique_ptr<ISplineFragment<Real>> clone_smart() const = 0;
  virtual ISplineFragment<Real> *clone() const = 0;
#ifndef SMOOTH_SPLINE_MINIMAL
  virtual std::string toString() const = 0;
#endif
};

template <typename Real>
struct EmptySplineFragment : public ISplineFragment<Real> {
  static_assert(std::is_floating_point<Real>::value,
                "Template argument mast be floating-point type");

  constexpr EmptySplineFragment()
      : nan(std::numeric_limits<Real>::quiet_NaN()) {}

  Real Y(Real x) const override { return nan; }
  Real dY(Real x) const override { return nan; }
  Real d2Y(Real x) const override { return nan; }
  Real Xmin() const override { return std::numeric_limits<Real>::infinity(); }
  Real Xmax() const override { return -std::numeric_limits<Real>::infinity(); }
  PointInFragment isXin(Real x) const override { return PointInFragment::IN; }
  std::unique_ptr<ISplineFragment<Real>> clone_smart() const override {
    return std::unique_ptr<ISplineFragment<Real>>(clone());
  }

  ISplineFragment<Real> *clone() const override {
    return new EmptySplineFragment<Real>();
  }

#ifndef SMOOTH_SPLINE_MINIMAL
  std::string toString() const override { return "EmptySplineFragment: { }"; }
#endif

  static std::unique_ptr<ISplineFragment<Real>> newEmptySplineFragment() {
    return std::unique_ptr<EmptySplineFragment<Real>>(new EmptySplineFragment);
  }

private:
  Real nan;
};

template <typename Real> struct SplineFragment : public ISplineFragment<Real> {
  static_assert(std::is_floating_point<Real>::value,
                "Template argument mast be floating-point type");

  Real A, B, C, D, m_Xmin, m_Xmax;

  SplineFragment(const Real A = 0, const Real B = 0, const Real C = 0,
                 const Real D = 0,
                 const Real Xmin = std::numeric_limits<Real>::min(),
                 const Real Xmax = std::numeric_limits<Real>::max())
      : A(A), B(B), C(C), D(D), m_Xmin(Xmin), m_Xmax(Xmax) {}

  Real Y(Real x) const override {
    x = x - m_Xmin;
    auto x2 = x * x;
    return A * x2 * x + B * x2 + C * x + D;
  }

  Real dY(Real x) const override {
    x = x - m_Xmin;
    return 3 * A * x * x + 2 * B * x + C;
  }

  Real d2Y(Real x) const override {
    x = x - m_Xmin;
    return 6 * A * x + 2 * B;
  }

  PointInFragment isXin(Real x) const override {
    if (x < m_Xmin)
      return PointInFragment::LESS;
    else if (x > m_Xmax)
      return PointInFragment::OVER;
    else
      return PointInFragment::IN;
  }

  Real Xmin() const override { return m_Xmin; }
  Real Xmax() const override { return m_Xmax; }

  std::unique_ptr<ISplineFragment<Real>> clone_smart() const override {
    return std::unique_ptr<ISplineFragment<Real>>(clone());
  }

  ISplineFragment<Real> *clone() const override {
    return new SplineFragment<Real>(*this);
  }

#ifndef SMOOTH_SPLINE_MINIMAL
  std::string toString() const override {
    std::ostringstream stringStream;
    stringStream << "SplineFragment: { " << std::setprecision(9)
                 << "Xmin: " << m_Xmin << ", Xmax: " << m_Xmax << ", A: " << A
                 << ", B: " << B << ", C: " << C << ", D: " << D << " }";
    return stringStream.str();
  }
#endif
};

} // namespace smsp

#endif /* _SPLINE_FRAGMENT_ */
