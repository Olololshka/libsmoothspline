# Библиотека построения сглаживающего сплайна через точки экспирементальных данных

# Требуется:
- `>=git-1.7` (Получение исходных кодов)
- `>=cmake-3.1` (Сборка)

## Ограничения
- Обязательно в исходных данных расстояниее по оси X между точками должно быть равно 1.0 иначе результат будет некорректным!
- По причине выше не подходит для данных, где точки распределены неравнеомерно по оси X


## C-версия

### Сборка:
* Скачать исхохный код при помощи git
`$ git clone git clone https://bitbucket.org/Olololshka/libmylog/libsmoothspline`

* Создаем каталог для сборки
`mkdir libsmoothspline/build && cd libsmoothspline/build`

* Запуск cmake
    - для windows:
        `$ cmake .. -G"MSYS Makefiles"`
    - для Linux:
        `$ cmake ..`

    - Сборка
        `make`

    - Установка (для Linux требуются права администратора).
      Каталог установки по-умолчанию `/usr/local`, что соответствует
      `<каталог установки MinGW>/msys/1.0/local` в Windows
        `# make install`

    - Сборка документации
        `$ make doc`

### Пример использования:
```c
#define SMOOTH_COEFF	(200) // коэфициент сглаживания
#define STEP			(0.1) // щаг между точками для прорисовки

...

// исходные данные
double Xdata[] = {...};
double Ydata[] = {...};

spline_range *spline; // сюда будет сохранена таблица коэфициентов сплайна
SplineApprox(Xdata, Ydata, sizeof(Xdata) / sizeof(double), SMOOTH_COEFF, &spline); // вычисление коэфициентов

...

double X = XStart; // начальное значение X, чтобы построить сплайн MIN(Xdata) <= XStart <= MAX(Xdata)
while(X <= XEnd) // MIN(Xdata) <= XEnd <= MAX(Xdata)
{
	calc_result result = calculateSplineAt(spline, sizeof(Xdata) / sizeof(double) - 1, X); // считаем
	printf("X = %f, Y = %f, d1y = %f, d2y = %f", X, result.Y, result.d1Y, result.d2Y); // вывод
    X += STEP;
}
```

## Rust-версия
Согласовано с [NSmoothSpline](bitbucket.org/Sctb_Elpa/kalibratorgui.git/NSmoothSpline)

### Cargo.toml
```toml
[dependencies]
libsmoothspline = { git = "https://bitbucket.org/Olololshka/libsmoothspline" }
```

### Пример использования:
```rust
// экспирименальные точки
let data = vec![...];

// Создание сплайна
let mut spline = SmoothSpline::<_, _, SplineFragment<_>>::new(&data);
spline.update(smooth_factor);

// Вычисление значений
let x = ...;
let y = spline.y(x);
let d1y = spline.d1y(x);
let d2y = spline.d2y(x);
```

### Примеры
1. Генерация данных и вывод точек в `stdout` в формате CSV
    `cargo run --example demo1` 
2. Примнмает в качестве файла набор точев в виде CSV `14.07.2017 09:20:57.538 <value>` и аппроксимирует их сплайном результат выводится в `stdout` в формате CSV
    `cargo run --example demo2 <file.csv> <smoothactor>`

